<?php

namespace App\Controller;

use App\Entity\Carte;
use App\Repository\CarteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/menu')]
class CarteVisiteurController extends AbstractController
{
    #[Route('/carte/visiteur', name: 'app_carte_visiteur')]
    // public function index(): Response
    // {
    //     return $this->render('carte_visiteur/index.html.twig', [
    //         'controller_name' => 'CarteVisiteurController',
    //     ]);
    // }

    public function index(CarteRepository $carteRepository): Response
    {
        return $this->render('carte_visiteur/index.html.twig', [
            'cartes' => $carteRepository->findAll(),
            
        ]);
    }
}
