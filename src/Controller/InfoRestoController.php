<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\InfoResto;
use App\Form\InfoRestoType;
use App\Repository\InfoRestoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Security("is_granted('ROLE_ADMIN')")]
#[Route('/info/resto')]
class InfoRestoController extends AbstractController
{
    #[Route('/', name: 'app_info_resto_index', methods: ['GET'])]
    public function index(InfoRestoRepository $infoRestoRepository): Response
    {
        return $this->render('info_resto/index.html.twig', [
            'info_restos' => $infoRestoRepository->findAll(),
        ]);
    }

    // #[Route('/new', name: 'app_info_resto_new', methods: ['GET', 'POST'])]
    // public function new(Request $request, InfoRestoRepository $infoRestoRepository): Response
    // {
    //     $infoResto = new InfoResto();
    //     $form = $this->createForm(InfoRestoType::class, $infoResto);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $infoRestoRepository->add($infoResto);
    //         return $this->redirectToRoute('app_info_resto_index', [], Response::HTTP_SEE_OTHER);
    //     }

    //     return $this->renderForm('info_resto/new.html.twig', [
    //         'info_resto' => $infoResto,
    //         'form' => $form,
    //     ]);
    // }

    // #[Route('/{id}', name: 'app_info_resto_show', methods: ['GET'])]
    // public function show(InfoResto $infoResto): Response
    // {
    //     return $this->render('info_resto/show.html.twig', [
    //         'info_resto' => $infoResto,
    //     ]);
    // }

    #[Route('/{id}/edit', name: 'app_info_resto_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, InfoResto $infoResto, InfoRestoRepository $infoRestoRepository): Response
    {
        $form = $this->createForm(InfoRestoType::class, $infoResto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $infoRestoRepository->add($infoResto);
            return $this->redirectToRoute('app_carte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('info_resto/edit.html.twig', [
            'info_resto' => $infoResto,
            'form' => $form,
        ]);
    }

    // #[Route('/{id}', name: 'app_info_resto_delete', methods: ['POST'])]
    // public function delete(Request $request, InfoResto $infoResto, InfoRestoRepository $infoRestoRepository): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$infoResto->getId(), $request->request->get('_token'))) {
    //         $infoRestoRepository->remove($infoResto);
    //     }

    //     return $this->redirectToRoute('app_info_resto_index', [], Response::HTTP_SEE_OTHER);
    // }
}
