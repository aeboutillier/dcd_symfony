<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\ListeResa;
use App\Form\ListeResaType;
use App\Repository\ListeResaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Security("is_granted('ROLE_ADMIN')")]
#[Route('/liste/resa')]
class ListeResaController extends AbstractController
{
    #[Route('/', name: 'app_liste_resa_index', methods: ['GET'])]
    public function index(ListeResaRepository $listeResaRepository): Response
    {
        return $this->render('liste_resa/index.html.twig', [
            'liste_resas' => $listeResaRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_liste_resa_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ListeResaRepository $listeResaRepository): Response
    {
        $listeResa = new ListeResa();
        $form = $this->createForm(ListeResaType::class, $listeResa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listeResaRepository->add($listeResa);
            return $this->redirectToRoute('app_liste_resa_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('liste_resa/new.html.twig', [
            'liste_resa' => $listeResa,
            'form' => $form,
            'liste_resas' => $listeResaRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_liste_resa_show', methods: ['GET'])]
    public function show(ListeResa $listeResa, ListeResaRepository $listeResaRepository): Response
    {
        return $this->render('liste_resa/show.html.twig', [
            'liste_resa' => $listeResa,
            'liste_resas' => $listeResaRepository->findAll(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_liste_resa_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ListeResa $listeResa, ListeResaRepository $listeResaRepository): Response
    {
        $form = $this->createForm(ListeResaType::class, $listeResa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listeResaRepository->add($listeResa);
            return $this->redirectToRoute('app_liste_resa_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('liste_resa/edit.html.twig', [
            'liste_resa' => $listeResa,
            'form' => $form,
            'liste_resas' => $listeResaRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_liste_resa_delete', methods: ['POST'])]
    public function delete(Request $request, ListeResa $listeResa, ListeResaRepository $listeResaRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$listeResa->getId(), $request->request->get('_token'))) {
            $listeResaRepository->remove($listeResa);
        }

        return $this->redirectToRoute('app_liste_resa_index', [], Response::HTTP_SEE_OTHER);
    }
}
