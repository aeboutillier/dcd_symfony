<?php

namespace App\Controller;


use App\Entity\ListeResa;
use App\Form\ListeResaType;
use App\Repository\ListeResaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/reservation')]
class ResaPageController extends AbstractController
{
    #[Route('/resa/page', name: 'app_resa_page')]
    // public function index(): Response
    // {
    //     return $this->render('resa_page/index.html.twig', [
    //         'controller_name' => 'ResaPageController',
    //     ]);
    // }
    public function new(Request $request, ListeResaRepository $listeResaRepository): Response
    {
        $listeResa = new ListeResa();
        $form = $this->createForm(ListeResaType::class, $listeResa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listeResaRepository->add($listeResa);
            return $this->redirectToRoute('app_resa_page', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('resa_page/index.html.twig', [
            'liste_resa' => $listeResa,
            'form' => $form,
            'liste_resas' => $listeResaRepository->findAll(),
        ]);
    }
}
