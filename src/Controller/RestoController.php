<?php

namespace App\Controller;

use App\Entity\InfoResto;
use App\Repository\InfoRestoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class RestoController extends AbstractController
{
    #[Route('/', name: 'app_resto')]
    // public function index(): Response
    // {
    //     return $this->render('resto/index.html.twig', [
    //         'controller_name' => 'RestoController',
    //     ]);
    // }

    public function index(InfoRestoRepository $infoRestoRepository): Response
    {
        return $this->render('resto/index.html.twig', [
            'info_restos' => $infoRestoRepository->find(5),
            
        ]);
    }
}
