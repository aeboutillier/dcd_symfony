<?php

namespace App\Entity;

use App\Repository\CategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategorieRepository::class)]
class Categorie
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $nom;


    #[ORM\OneToMany(mappedBy: 'cat', targetEntity: Carte::class)]
    private $prodcart;

    public function __construct()
    {
       
        $this->prodcart = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Carte>
     */
    public function getProdcart(): Collection
    {
        return $this->prodcart;
    }

    public function addProdcart(Carte $prodcart): self
    {
        if (!$this->prodcart->contains($prodcart)) {
            $this->prodcart[] = $prodcart;
            $prodcart->setCat($this);
        }

        return $this;
    }

    public function removeProdcart(Carte $prodcart): self
    {
        if ($this->prodcart->removeElement($prodcart)) {
            // set the owning side to null (unless already changed)
            if ($prodcart->getCat() === $this) {
                $prodcart->setCat(null);
            }
        }

        return $this;
    }
}
